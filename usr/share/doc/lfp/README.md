
# An lf wrapper, with built-in previews and on-exit directory changing.

### `lfp` is a simple wrapper for the `lf` file manager, with built-in file, image, and video previews, as well as on-exit directory changing.
### The program makes use of `ueberzug` to preview images, documents, and video thumbnails. Preview behavior can be altered by editing the *scope* file.

**Version: 4.1.2**

***

![previews](https://gitlab.com/a4to/lfp/-/raw/master/.gitlab/media/previews.gif)

## Usage:

Run `lfp`. Optionally, a startup directory can be specified by a second argument. By default, `lfp` will launch in your current working directory.
Icons for relative file types can be changed by editing the lfp-icons file in *'/usr/share/lfp/lfp-icons'*.

**CD on exit:**

For on-exit directory changing, add the following line to your bash/zshrc:

    source '/usr/share/lfp/lfpcd'


**LFP OCR:**

LFP OCR is a simple OCR script that uses tesseract to extract text from images. It can be used as a standalone script, or as a lfp action.
All detected text is copied to your clipboard.


![OCR](https://gitlab.com/a4to/lfp/-/raw/master/.gitlab/media/OCR.gif)


## Installation

### Arch Linux

lfp is available in the AUR as `lfp` or `lfp-git`


### Manual Installation

    tmp=$(mktemp -d);
    git clone https://gitlab.com/a4to/lfp.git ${tmp} &&
    cd ${tmp} && sudo make clean install && cd - &&
    rm -rf ${tmp} >/dev/null 2>&1 || true


**To uninstall lfp:**

    sudo make uninstall



## Prerequisites:

* `lf`: the file manager
* `zsh`: for scripting
* `ueberzug`: for image previews
* `ghostscript`: for PDF previews
* `graphicsmagick`: for SVG and GIF previews
* `ffmpeg`: for video file thumbnail previews
* `dialog`: required by many functions outside the scope of image and video previews
* `dunst`: to receive notifications upon task completion and so forth
* `fzf`: for lfp's *fzfp* (fuzzy-finding) and *pushto* (git push) capabilities
* `bat`: for text file previews
* `git`: for cloning repos from within lfp
* `lolcat`: used in lfp's *fzfp* function

## Optional Dependencies:

* `nodejs`: to make use of lfps built in node actions manager
* `python3`: to make use of lfps built in python actions manager



## Keybindings

### Navigation:

* **h**: Left
* **j**: Down
* **k**: Up
* **l**: Right


### Main Features and Functions:

* **.**: Activate NodeManager
* **,**: Activate PyManager
* **o**: Activate LFP OCR
* **v**: Toggle select all file(s)
* **y**: Copy selected file(s)
* **c**: Cut selected file(s)
* **d**: Delete selected file(s)
* **p**: Paste selected file(s)
* **d**: Delete selected file(s)
* **s**: Edit file you're currently hovering over
* **n**: Create new directory
* **m**: Play with mpv
* **f**: Activate fzfp (lfp's fuzzy-finder)
* **b**: Open file in default browser
* **e**: Prompts you for a filename to edit/create
* **t**: Prompts you for a filename for blank file to create
* **x**: Execute selected file(s)
* **z**: Open in `sxiv` (an image viewer)
* **q**: Quit lfp
* **\<Space\>**: Select file(s)
* **\<Period\>**: (.) - Activates Node Actions, various npm/yarn actions / quick command execution


### NodeManager:

The NodeManager function provides various actions to manage a Node.js project, including:

![NodeManager](https://gitlab.com/a4to/lfp/-/raw/master/.gitlab/media/NodeManager.gif)

* **↑ Arrow**: Initialize a package.json file.
* **↓ Arrow**: Select a javascript file to run.
* **← Arrow**: Specify package(s) to install from npm.
* **→ Arrow**: Start the server (yarn start).
* **[h|d|m]**:: Start the server in development mode (yarn dev).
* **[b|n]**: Build the project (yarn build).
* **[i|I|,]**: Initialize a new npm project.
* **[r]**: Remove specified packages.
* **.**: Update the server (yarn).
* **/**: Create a webserver on port 3000 (requires serve).
* **q**: Exit.


### PyManager:

The PyManager function provides various actions to manage a Python project, including:

![PyManager](https://gitlab.com/a4to/lfp/-/raw/master/.gitlab/media/PyManager.gif)

* **↑ Arrow**: Create a new Conda environment at "https://gitlab.com/a4to/lfp/-/raw/master/.🐍".
* **↓ Arrow**: Select a python script to run.
* **← Arrow**: Select package(s) to install from pypi.
* **→ Arrow**: Run the main.py script if found.
* **q**: Exit.


### Additional Actions and Features:

* **B**: Bulk rename
* **E**: Extract archive
* **D**: (sudo) Delete selected file(s)
* **L**: Symlink selected file(s) to current directory
* **H**: Create hardlink of selected file(s) to current directory
* **R**: Create relative symlink of selected file(s) to current directory
* **E**: Extract archive (bz2 rar gz tar tar.* tbz2 tgz zip Z 7z deb)
* **C**: CopyPath, this feature copies the absolute path of the file youre currently hovering over to your clipboard
* **\<Ctrl+c\>**: Quick clone a git repository without leaving lfp
* **\<Ctrl+g\>**: Activate pushto - select a branch to push to (only works inside a git repo)
* **\<Ctrl+b\>**: Open file in browser
* **\<Ctrl+6\>**: Chmod 644 selected file(s)
* **\<Ctrl+7\>**: Chmod 755 selected file(s)
* **\<Ctrl+8\>**: Chmod 700 selected file(s)
* **\<Enter\>**: Enter shell command


And many more.


### File Type Icons:

![fileTypes](https://gitlab.com/a4to/lfp/-/raw/master/.gitlab/media/fileTypes.gif)

