#!/usr/bin/env node

const fs = require('fs');
const fsp = require('fs').promises;
const path = require('path');
const tesseract = require('./tesseract');
const { spawn } = require('child_process');

const green = '\x1b[32m';
const reset = '\x1b[0m';

const config = { lang: 'eng', oem: 1, psm: 3 };
const { exec } = require('child_process');

const sh = (cmd, opts) => new Promise((resolve, reject) => {
  exec(cmd, opts, (err, stdout, stderr) => {
    if (err) {
      reject(err);
    } else {
      resolve(stdout.trim());
    }
  });
});

let fail = false;

const ocr = async (imagePath) => {
  const Type = await sh(`file -b --mime-type ${imagePath}`);
  if (await sh(`file -bL --mime-encoding ${imagePath}`) === 'utf-8') {
    return await fsp.readFile(imagePath, 'utf8');
  } else if (Type === 'PDF document') {
    await sh(`convert ${imagePath} -background white ${imagePath}.png`);
    const res = await tesseract.recognize(`${imagePath}.png`, config);
    await fs.unlinkSync(`${imagePath}.png`);
    return res;
  } else if(Type === 'PNG image data' || Type === 'JPEG image data' || Type === 'TIFF image data' || Type === 'image/x-portable-bitmap' || Type === 'image/png' || Type === 'image/jpeg' || Type === 'image/tiff' || Type === 'PC bitmap') {
    const image = await fsp.readFile(imagePath);
    return await tesseract.recognize(image, config);
  } else {
    fail = true;
    throw new Error('Unknown image type');
  }
}

const args = process.argv.slice(2);
const writeFile = (file, data) => fs.writeFile(file, data, (err) => { if (err) console.error(err); });
const timestamp = () => new Date().toLocaleDateString('en-GB', { day: '2-digit', month: 'long', year: 'numeric', hour: '2-digit', minute: '2-digit', second: '2-digit' });
const extractsDir = '/home/nvx1/.local/share/logs/ocrListener/extracts';


(async () => {
  const texts = await Promise.all(args.map(async arg => {
    const imagePath = path.resolve(process.cwd(), arg);
    try {
      return await ocr(imagePath);
    } catch (err) {
      console.error(err);
      return '';
    }
  }));

  const combinedText = texts.join('\n');

  const echo = spawn('echo', [combinedText]);
  const xclip = spawn('xclip', ['-selection', 'clipboard']);

  new Promise((resolve, reject) => {
    if (!fs.existsSync(extractsDir)) {
      fs.mkdirSync(extractsDir);
    }
    resolve(writeFile(path.join(extractsDir, `${timestamp().replace(/:/g, '-').replace(/ /g, '_')}.txt`), combinedText));
  }).then(() => {

    if (!fail) {
      console.log(`\n${green}[+] ${reset}LFP OCR:\n`);

      echo.stdout.pipe(xclip.stdin);

      echo.on('close', () => {
        console.log(combinedText);
        process.exit(0);
      });
    } else process.exit(1);
  })

})();

