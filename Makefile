
PKG = lfp
PREVIEWER = lfpreviewer

INFO = $(shell uname -a)
OST = $(shell lsb_release -d | sed 's|Description:\s*||')
OS = $(shell lsb_release -d | sed 's|Description:\s*||'|cut -d\  -f1)

ifeq ($(OS),Ubuntu)
	PACKAGE_MANAGER = apt
	INSTALL = sudo ${PACKAGE_MANAGER}-get install
	QUERY = sudo dpkg -S >/dev/null 2>&1
	PKGS = python3-pip python3-setuptools libxres1
endif

ifeq ($(OS),Debian)
	PACKAGE_MANAGER = apt
	INSTALL = sudo ${PACKAGE_MANAGER}-get install
	QUERY = sudo dpkg -S >/dev/null 2>&1
	PKGS = python3-pip python3-setuptools libxres1
endif

ifeq ($(OS),Arch)
	PACKAGE_MANAGER = pacman
	INSTALL = sudo ${PACKAGE_MANAGER} -Syy
	QUERY = sudo ${PACKAGE_MANAGER} -Qq >/dev/null 2>&1
	PKGS = python-pip python-setuptools libxres
endif

ifeq ($(PACKAGE_MANAGER),)
$(error >   Unsupported OS: ${OST})
endif

all: info

lfpreviewer:
	@echo -e "\n\e[1;33m[+]\e[0m Installing ${PREVIEWER}...\n"
	@rm -rf ${PREVIEWER}/build ${PREVIEWER}/${PREVIEWER}.egg-info ${PREVIEWER}/dist
	@${QUERY} ${PKGS} || ${INSTALL} ${PKGS}
	@python3 -m pip install --upgrade pip
	@python3 -m pip install --upgrade setuptools
	@cd ${PREVIEWER} && python3 ./setup.py install && cd .. || true
	@which lfpreviewer >/dev/null 2>&1 || echo -e "\n\e[1;31m[-]\e[0m Please ensure you have sufficient premission to install ${PREVIEWER}."

lfp:
	@echo -e "\n\e[1;33m[+]\e[0m Installing ${PKG}...\n"
	@sudo install -Dm755 usr/bin/* -t "${PKGDIR}/usr/bin" || true
	@sudo install -Dm755 usr/share/${PKG}/{${PKG},${PKG}cd,cleaner,scope} -t "${PKGDIR}/usr/share/${PKG}" || true
	@sudo install -Dm644 usr/share/${PKG}/{${PKG}-icons,${PKG}rc} -t "${PKGDIR}/usr/share/${PKG}" || true
	@sudo install -Dm644 usr/share/licenses/${PKG}/LICENSE "${PKGDIR}/usr/share/licenses/${PKG}/LICENSE" || true
	@sudo install -Dm644 usr/share/doc/${PKG}/README.md "${PKGDIR}/usr/share/doc/${PKG}/README.md" || true
	@sudo install -Dm644 usr/share/man/man1/${PKG}.1.gz "${PKGDIR}/usr/share/man/man1/${PKG}.1.gz" || true
	@[ ! -d /usr/share/lfp ] && echo -e "\n\e[1;31m[-]\e[0m Please ensure you have sufficient premission to install ${PKG}." || true

install: lfpreviewer lfp cd-on-exit

clean:
	@echo -e "\n\e[1;33m[+]\e[0m Cleaning up..."
	@sudo rm -rf ${PKGDIR}/lib/python3.*/site-packages/${PREVIEWER}*
	@sudo rm -rf ${PKGDIR}/usr/bin/${PREVIEWER}
	@sudo rm -rf ${PKGDIR}/usr/share/${PKG}
	@sudo rm -rf ${PKGDIR}/usr/bin/${PKG}
	@sudo rm -rf ${PKGDIR}/usr/bin/${PKG}cd
	@sudo rm -rf ${PKGDIR}/usr/bin/cleaner
	@sudo rm -rf ${PKGDIR}/usr/bin/scope
	@sudo rm -rf ${PKGDIR}/usr/share/licenses/${PKG}
	@sudo rm -rf ${PKGDIR}/usr/share/doc/${PKG}
	@sudo rm -rf ${PKGDIR}/usr/share/man/man1/${PKG}.1.gz

info:
	@echo -e "\n\e[1;33m[*]\e[0m ${INFO}"

help:
	@echo -e "\n	Usage: make [all|install|uninstall|clean|distclean|preview|help]\n"
	@echo "	     all: build $(PKG)"
	@echo "	     install: install $(PKG)"
	@echo "	     uninstall: uninstall $(PKG)"
	@echo "	     clean: clean $(PKG)"
	@echo "	     distclean: clean $(PKG) and remove $(PKG) directory"
	@echo "	     preview: preview $(PKG)"
	@echo "	     help: show this help"

cd-on-exit:
	@[ -d /usr/share/lfp ] && echo -e "\n\n\033[1;32m[+]\e[0;1m For on-exit directory changing, add the following line to your bash/zshrc\e[0m :\e[36m\n\n   source '/usr/share/lfp/lfpcd'\n\e[0m" || echo -e "\n\033[1;31m[!]\e[0;1m Installation Failed!"

uninstall: clean
	@echo -e "\n\e[1;32m[+]\e[0m ${PKG} successfully uninstalled"

remove: uninstall

.PHONY: info all lfpreviewer lfp install clean help cd-on-exit uninstall remove

